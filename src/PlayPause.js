import React, { Component } from 'react'
import PlayButton from './Components/PlayButton'
import PauseButton from './Components/PauseButton'
import PubSub from 'pubsub-js';

class PlayPause extends Component {

  handlePlayClick() {
    console.log("send command: play");
    PubSub.publish('SendCommand', "Play");
    //this.setState({isPlaying: true});
  }
  handlePauseClick() {
    console.log("send command: pause");
    PubSub.publish('SendCommand', "Pause");
    // this.setState({isPlaying: false});
  }

  render() {
    const isPlaying = this.props.isPlaying;
    if (isPlaying) {
      return <PauseButton onClick={this.handlePauseClick}  />;
    } else {
      return <PlayButton onClick={this.handlePlayClick}  />;
    }
  }
}

export default PlayPause