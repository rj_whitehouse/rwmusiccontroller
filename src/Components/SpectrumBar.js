import React, { Component } from 'react'
/*import PlayButton from './Components/PlayButton'
import PauseButton from './Components/PauseButton'*/
import PubSub from 'pubsub-js';

class SpectrumBar extends Component {
  render() {
    const left = this.props.counter * 20;

    return (
      <div key={this.props.barKey} className="spectrum"  style={{ height: this.props.height, left: left }}/>
    );

  }
}

export default SpectrumBar