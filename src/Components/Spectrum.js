import React, { Component } from 'react'
import SpectrumBar from './SpectrumBar'
import PubSub from 'pubsub-js';

class Spectrum extends Component {
  render() {
    return(
      this.props.spectrumValues.map((gh, i)=>(
      <SpectrumBar key={this.props.id + i} barKey={this.props.id + i} counter={i} height={gh.height}  />
    )));
  }
}

export default Spectrum