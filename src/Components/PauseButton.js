import React, { Component } from 'react';

export default class PauseButton extends Component {
  constructor(props) {
    super(props);
  }
  render (state) {
    return (<button onClick={this.props.onClick}>
        Pause
      </button>);
  }
}