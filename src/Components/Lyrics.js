import React, { Component } from 'react'

class Lyrics extends Component {
  render() {
    return(
      this.props.lyrics.map((gh, i)=>(
        <div>{gh.Lyric.replace('\\n','<br />')}</div>
      )));
  }
}

export default Lyrics