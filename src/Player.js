import React, { Component } from 'react'
import PlayPause from './PlayPause'
import Spectrum from './Components/Spectrum'
import Lyrics from './Components/Lyrics'
import PubSub from 'pubsub-js';

// const URL = 'ws://192.168.99.1:1101'
const URL = 'ws://192.168.1.107:1104'

class Player extends Component {
  state = {
    name: 'Bob',
    messages: [],
    spectrumValues: [],
    playerStatus: {},
    currentLyrics: []
  }

  ws = new WebSocket(URL);

  componentDidMount() {
    this.ws.onopen = () => {
      // on connecting, do nothing but log it to the console
      console.log('connected')
      console.log(this);
      this.sendCommandSubscriber("","PlayerStatus");
    }

    this.ws.onmessage = evt => {
      // console.log("received message!");
      // console.log(evt.data);
      // on receiving a message, add it to the list of messages
      const message = JSON.parse(evt.data)
      // console.log(message.name);
      if (message.name === "SpectrumValues") {
        this.setState({spectrumValues: message.spectrumValues });
      } else if (message.name === "PlayerStatus") {
        console.log("Player Status Received");
        console.log(message);
        this.setState({playerStatus: message.playerStatus });
      } else if (message.name === "CurrentLyrics") {
        console.log("Lyrics Received");
        console.log(message);
        this.setState({currentLyrics: message.lyrics });
      } else {
        this.addMessage(message)
      }
    }

    this.ws.onclose = () => {
      console.log('disconnected')
      // automatically try to reconnect on connection loss
      this.setState({
        ws: new WebSocket(URL),
      })
    }

    this.ws.onerror = evt => {
      console.log("an error has occured!");
    }

    this.token = PubSub.subscribe('SendCommand', this.sendCommandSubscriber.bind(this));
  }

  sendCommandSubscriber (msg, command){
    console.log("I've received a command I should send! " + command);
    const commandMessage = { action: command };
    if (command === "Pause") {
      this.setState({
        isPlaying: false
      })
    } else {
      this.setState({
        isPlaying: true
      })
    }
    this.sendMessage(commandMessage);
  }

  addMessage = message =>
    this.setState(state => ({ messages: [message, ...state.messages] }))

  submitMessage = messageString => {
    // on submitting the ChatInput form, send the message, add it to the list and reset the input
    console.log("message sent!");
    const message = { name: this.state.name, message: messageString }
    console.log(message);
    this.ws.send(JSON.stringify(message))
    // this.ws.send(JSON.stringify('0000000000000000'))
    console.log(this.ws);
    this.addMessage(message)
  }

  sendMessage = messageString => {
    console.log(messageString);
    this.ws.send(JSON.stringify(messageString));
  }

  submitCommand = commandString => {
    // on submitting the ChatInput form, send the message, add it to the list and reset the input
    const command = { command: this.state.name, message: commandString }
    console.log(command);
    this.ws.send(JSON.stringify(command))
    // this.ws.send(JSON.stringify('0000000000000000'))
    console.log(this.ws);
    this.addMessage(command)
  }

  render() {
    // console.log(this.state.isPlaying + " is the playing state");
    return (
      <div>
        <PlayPause
          ws={this.ws}
          isPlaying={this.state.playerStatus.isPlaying}
          onSubmitMessage={messageString => this.submitMessage(messageString)}
        />
        <p>{this.state.playerStatus.artist} - {this.state.playerStatus.songName}</p>

        <div className="lyrics">
          <Lyrics
            ws={this.ws}
            lyrics={this.state.currentLyrics}
          />
        </div>

        <div className="spectrumValues">
          <Spectrum ws={this.ws} id="fwd" spectrumValues={this.state.spectrumValues}/>
        </div>
        <div className="spectrumValues">
          <Spectrum ws={this.ws} id="rev" spectrumValues={[...this.state.spectrumValues].reverse()}/>
        </div>
        
      </div>
    )
  }
}

export default Player